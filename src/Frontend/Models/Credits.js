function Credits(credits) {
    if(credits !== undefined) {
        this.id = credits.id;
        this.name = credits.name;
        this.park = credits.park;
    }
}

export default Credits;