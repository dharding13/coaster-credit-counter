function BucketList(bucketList) {
    if(bucketList !== undefined) {
        this.id = bucketList.id;
        this.name = bucketList.name;
        this.park = bucketList.park;
    }
}

export default BucketList;