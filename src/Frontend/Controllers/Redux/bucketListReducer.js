import {createSlice} from "@reduxjs/toolkit";

const BucketListReducer = createSlice({
    name: "bucketList",
    initialState: [{}],
    reducers: {
        getBucketList: (state) => {
            state.push({id: ""})
            state.push({name: ""})
            state.push({park: ""})
        }
    }
})

export default BucketListReducer.reducer

export const {getBucketList} = BucketListReducer.actions