import {createSlice} from "@reduxjs/toolkit";

const UserReducer = createSlice({
    name: "user",
    initialState: [{}],
    reducers: {
        getUser: (state) => {
            state.push({id: ""})
            state.push({firstName: ""})
            state.push({lastName: ""})
            state.push({username: ""})
            state.push({email: ""})
            state.push({password: ""})
        }
    }
})

export default UserReducer.reducer

export const {getUser} = UserReducer.actions