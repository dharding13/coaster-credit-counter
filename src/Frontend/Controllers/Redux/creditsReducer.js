import {createSlice} from "@reduxjs/toolkit";

const CreditsReducer = createSlice({
    name: "credits",
    initialState: [{}],
    reducers: {
        getCredits: (state) => {
            state.push({id: ""})
            state.push({name: ""})
            state.push({park:""})
        }
    }
})

export default CreditsReducer.reducer

export const {getCredits} = CreditsReducer.actions