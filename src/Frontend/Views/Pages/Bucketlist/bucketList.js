import React from "react";
import "./bucketList.css"

function BucketList() {
    return(
        <div className="bucketListPanel">
            <form className="bucketListAdd">
                <h1>Add new bucket list coaster</h1>
                <input type="coaster" placeholder="Coaster Name" />
                <input type="park" placeholder="Park" />
                <button type="add">Add coaster</button>
            </form>
        </div>
    )
}

export default BucketList;