import React from "react";
import "./heroPage.css";
import {Link} from "react-router-dom";

function HeroPage() {
    return (
        <div>
            <iframe src="https://player.vimeo.com/video/572707120?autoplay=1&loop=1"
                    width="1980"
                    height="1080"
                    frameBorder="0"
                    allow="autoplay; fullscreen; picture-in-picture"
                    allowFullScreen></iframe>
            <div className="content">
                <h1>Welcome to Coaster Credit Counter</h1>
                <p>Join your fellow coaster enthusiasts and keep track of your credits and bucket list.</p>
                <Link to="/login">
                    <button type="button" className="loginButton">Click here to log in</button>
                </Link>
            </div>
            <footer className="homeFooter">

            </footer>
        </div>

    )
}

export default HeroPage;

