import React from "react";

function Registration(props) {

    const redirectToLogin = () => {
        props.history.push("/login");
    }

    return(
        <div>
            <form>
                <input type="firstname" placeholder="First Name" />
                <input type="lastname" placeholder="Last Name" />
                <input type="email" placeholder="Email Address" />
                <input type="username" placeholder="Username" />
                <input type="pasword" placeholder="Password" />
                <input placeholder="Confirm Password" />
                <button type="submit">Register</button>
            </form>
            <span>Already have an account?</span>
            <span onClick={() => redirectToLogin()}><button type="login">Login Here</button></span>
        </div>
    )
}

export default Registration;