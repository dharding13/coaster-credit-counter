import React, {useState} from "react";
import {useDispatch} from "react-redux";
import {signIn} from "../../../Controllers/Redux/authReducer";
import "./Login.css";

function Login(props) {

    const dispatch = useDispatch();

    const [formInput, setFormInput] = useState({
        name: "",
        password: ""
    })

    function inputChanged(e) {
        setFormInput({
            ...formInput,
            [e.target.name]: e.target.value
        })
    }

    function Submit(e) {
        dispatch(signIn(formInput));
        e.preventDefault();
    }

    const redirectToRegister = () => {
        props.history.push("/register");
    }

    return (
        <div className="loginPanel">
            <form className="loginForm">
                <h1>Login</h1>
                <input name="name" placeholder="Name" onChange={inputChanged} value={formInput.name}/>
                <input name="password" placeholder="Password" onChange={inputChanged} value={formInput.password}/>
                <button type="submit" onClick={Submit}>Login</button>
                <span>Don't have an account?</span>
                <span className="loginText" onClick={() => redirectToRegister()}><button
                    type="register">Register</button></span>

            </form>
        </div>
    )
}

export default Login;