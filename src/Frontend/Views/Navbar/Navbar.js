import React from "react";
import "./Navbar.css";
import {BrowserRouter as Router, Link} from "react-router-dom";

function Navbar() {
    return (
        <div>
            <Router>
                <nav className="navbar navbar-expand-lg navbar-light bg-primary">
                    <div className="container-fluid">

                        <button className="navbar-toggler" type="button" data-bs-toggle="collapse"
                                data-bs-target="#navbarText" aria-controls="navbarText" aria-expanded="false"
                                aria-label="Toggle navigation">
                            <span className="navbar-toggler-icon"></span>
                        </button>
                        <div className="collapse navbar-collapse" id="navbarText">
                            <ul className="navbar-nav me-auto mb-2 mb-lg-0">
                                <li className="nav-item">
                                    <Link to="/home" className="nav-link active" aria-current="page">Home</Link>
                                </li>
                                <li className="nav-item">
                                    <Link to="/mycredits" className="nav-link">My Credits</Link>
                                </li>
                                <li className="nav-item">
                                    <Link to="/bucketlist" className="nav-link">Bucketlist</Link>
                                </li>
                                <li className="nav-item">
                                    <Link to="/login" className="nav-link">Login</Link>
                                </li>
                            </ul>
                            <span className="navbar-text"></span>
                        </div>
                    </div>
                </nav>
            </Router>
        </div>
    )
}

export default Navbar;