import React from 'react';
import ReactDOM from 'react-dom';
import {BrowserRouter, Route} from 'react-router-dom'
import 'mdb-react-ui-kit/dist/css/mdb.min.css'
import './index.css';
import App from './App';
import {combineReducers, configureStore} from "@reduxjs/toolkit";
import AuthReducer from "./Frontend/Controllers/Redux/authReducer";
import {Provider} from "react-redux";
import userReducer from "./Frontend/Controllers/Redux/userReducer";
import creditsReducer from "./Frontend/Controllers/Redux/creditsReducer";
import bucketListReducer from "./Frontend/Controllers/Redux/bucketListReducer";

const reducer = combineReducers({
    auth: AuthReducer,
    user: userReducer,
    credits: creditsReducer,
    bucketList: bucketListReducer
})

const store = configureStore({
    reducer
})

ReactDOM.render(
    <Provider store={store}>
        <BrowserRouter>
            <Route path="/" component={App}>
            </Route>
        </BrowserRouter>
    </Provider>
    ,
    document.getElementById('root')
);