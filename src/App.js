import {BrowserRouter as Router, Link, Redirect, Route, Switch} from "react-router-dom";
import Credits from "./Frontend/Views/Pages/Credits/Credits";
import BucketList from "./Frontend/Views/Pages/Bucketlist/bucketList";
import Login from "./Frontend/Views/Pages/Login/Login";
import HeroPage from "./Frontend/Views/Pages/heroPage/heroPage";
import Registration from "./Frontend/Views/Pages/Registration/Registration";

function App() {
  return (
    <div className="App">


        <Router>
            <nav class="navbar navbar-expand-lg navbar-light bg-primary">
                <Link to="/home" class="navbar-brand">Coaster Credit Counter</Link>
                <Link to="/login" class="navbar-brand">Login</Link>
                <Link to="/mycredits" class="navbar-brand">My Credits</Link>
                <Link to="/bucketlist" class="navbar-brand">Bucket List</Link>
            </nav>
            <Switch>
                <Route exact path="/">
                    <Redirect to= "/home"></Redirect>
                </Route>
                <Route path="/home">
                    <HeroPage />
                </Route>
                <Route path="/mycredits">
                    <Credits />
                </Route>
                <Route path="/bucketlist">
                    <BucketList />
                </Route>
                <Route path="/login">
                    <Login />
                </Route>
                <Route path="register">
                    <Registration />
                </Route>
            </Switch>
        </Router>
    </div>
  );
}

export default App;
